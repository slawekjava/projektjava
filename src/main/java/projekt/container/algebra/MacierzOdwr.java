/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt.container.algebra;



/**
 *
 * @author Indzia
 */
public class MacierzOdwr extends Macierz implements Wypisywalny
{


    public MacierzOdwr(double [][]m) 
    {
        super(m);
    }
    
    public MacierzOdwr odwroc()
    {
        double det = getA()[0][0] * getA()[1][1] - getA()[0][1] * getA()[1][0];
        MacierzOdwr tmp = null;
        if(det != 0) 
        {
            tmp = new MacierzOdwr(new double[][] 
                                            {
                                            {getA()[1][1]/det, -getA()[0][1]/det},
                                            {-getA()[1][0]/det, getA()[0][0]/det}
                                            }
                                  );
        }
        return tmp;
    }
}
