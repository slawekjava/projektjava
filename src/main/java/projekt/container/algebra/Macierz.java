/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt.container.algebra;


/**
 *
 * @author Indzia
 */
public class Macierz extends Algebra implements Dzialaj, Wypisywalny
{
    private double[][] a;
    public  Macierz() {setA(new double[2][2]);}
    public Macierz(double[][] am) {
        setA(new double[2][2]);
        int i,j;
        for(i=0; i<2; i++) {
            for(j=0; j<2; j++) {
                getA()[i][j] = am[i][j];
            }
        }
    }

    @Override
    public void dodaj(Object o) 
    {
        Macierz m = (Macierz)o;
        
        for(int i=0; i<getA().length; i++)
        {
            for(int j=0; j<getA()[i].length; j++)
            {
                getA()[i][j] += m.getA()[i][j];
            }
        }
    }

    @Override
    public void odejmij(Object o) 
    {
        Macierz m = (Macierz)o;
        
        for(int i=0; i<getA().length; i++)
        {
            for(int j=0; j<getA()[i].length; j++)
            {
                getA()[i][j] -= m.getA()[i][j];
            }
        }
    }

    @Override
    public void pomnoz(Object o) 
    {
        Macierz m = (Macierz)o;
        
        for(int i=0; i<getA().length; i++)
        {
            for(int j=0; j<getA()[i].length; j++)
            {
                getA()[i][j] *= m.getA()[i][j];
            }
        }
    }

    @Override
    public void wypisz() 
    {
        for(int i=0; i<getA().length; i++)
        {
            System.out.print("\t\t\t\t\t| ");
            for(int j=0; j<getA()[i].length; j++)
            {
                System.out.print(getA()[i][j] + " ");
            }
            System.out.print("| \n ");
        }
        System.out.println();
    }
    
    
    public String wypisz2() 
    {
        StringBuilder tmp = new StringBuilder("\n");
        for(int i=0; i<getA().length; i++)
        {
            tmp.append("\t\t                             | ");
            for(int j=0; j<getA()[i].length; j++)
            {
                tmp.append( String.format(getA()[i][j] + " "));
            }
            tmp.append( " |\n" );
        }
        
        return tmp.toString();
    }
	public double[][] getA() {
		return a;
	}
	public void setA(double[][] a) {
		this.a = a;
	}
}
