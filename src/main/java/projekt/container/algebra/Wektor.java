/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt.container.algebra;


public class Wektor extends Algebra implements Dzialaj, Wypisywalny {
	private double[] x = new double[3];

	public Wektor() {
	};

	double dlugosc;

	public Wektor(double x1, double x2, double x3) {
		getX()[0] = x1;
		getX()[1] = x2;
		getX()[2] = x3;
	}

	public Wektor(int zakres) {
		inicjalizujLosowo(zakres);
	}

	private void inicjalizujLosowo(int zakres) {
		for (int i = 0; i < 3; i++) {
			getX()[i] = (int) ((Math.random() * zakres) + 0.5);
		}
	}

	@Override
	public void dodaj(Object o) {
		Wektor w1 = (Wektor) o;

		for (int i = 0; i < getX().length; i++) {
			getX()[i] += w1.getX()[i];
		}
	}

	@Override
	public void odejmij(Object o) {
		Wektor w1 = (Wektor) o;

		for (int i = 0; i < getX().length; i++) {
			getX()[i] -= w1.getX()[i];
		}
	}

	@Override
	public void pomnoz(Object o) {
		Wektor w1 = (Wektor) o;

		for (int i = 0; i < getX().length; i++) {
			getX()[i] *= w1.getX()[i];
		}
	}

	public double obliczDlougosc() {
		dlugosc = Math.sqrt(Math.pow(getX()[0], 2) + Math.pow(getX()[1], 2)
				+ Math.pow(getX()[2], 2));
		return dlugosc;
	}

	@Override
	public void wypisz() {
		System.out.println("\t\t\t\t     [ " + getX()[0] + ", " + getX()[1] + ", " + getX()[2]
				+ " ]");
		if (dlugosc != 0) {
			System.out.println("\t\t\t\t Dlugosc = " + dlugosc);
		}
	}

	public String wypisz1() {
		StringBuilder tmp = new StringBuilder("");
		tmp.append(String.format("\n\t\t                     [ " + getX()[0] + ", "
				+ getX()[1] + ", " + getX()[2] + " ]\n"));

		if (dlugosc != 0) {
			tmp.append("\t\t       Dlugosc = " + dlugosc);
		}
		return tmp.toString();

	}

	public double[] getX() {
		return x;
	}

	public void setX(double[] x) {
		this.x = x;
	}
}
