package projekt.container.algebra;

public interface Dzialaj {
	abstract void dodaj(Object o);

	abstract void odejmij(Object o);

	abstract void pomnoz(Object o);
}