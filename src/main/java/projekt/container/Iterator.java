package projekt.container;

public interface Iterator {
	public boolean hasNext();

	public Object next();
}
