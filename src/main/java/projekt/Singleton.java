package projekt;

public class Singleton {
	private static volatile Singleton singleton = null;

	static Singleton getSingleton() {
		if (singleton == null) {
			synchronized (Singleton.class) {
				if (singleton == null) {
					singleton = new Singleton();
				}
			}
		}
		return singleton;
	}
	
	private Singleton() {}

	public void powitanie() {
		System.out
				.println("############################ Dzien dobry oto moj projket z JAVY ############################ \n");
	}

	public void napisy() {
		System.out.println("\t\t\t\t\t\t\tWykonali: Sławek Hałka i Diana Gadula\n");
	}

}
