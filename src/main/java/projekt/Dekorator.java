package projekt;

import projekt.container.algebra.Wersor;

public abstract class Dekorator extends Wersor {
	public abstract String about_wersor();
}
