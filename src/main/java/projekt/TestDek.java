package projekt;

import projekt.container.algebra.Wersor;

public class TestDek extends Dekorator {

	Wersor wersor;

	public TestDek(Wersor wersor) {
		this.wersor = wersor;
	}

	@Override
	public String about_wersor() {
		return wersor.about_wersor() + "   TEST DEKORATORA";
	}

}
